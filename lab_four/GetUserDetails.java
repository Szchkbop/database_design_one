//package com.journaldev.jdbc.statements;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.util.Scanner;
import java.sql.DriverManager;


public class GetUserDetails {
        public final static String DB_DRIVER_CLASS = "com.mysql.jdbc.Driver";
        public final static String DB_URL = "jdbc:mysql://groucho.it.uu.se:3306/ht16_hoteldb_it22";
        public final static String DB_USERNAME = "ht16_user_it22";
        public final static String DB_PASSWORD = "pwd_it22";



	public static void main(String[] args)
 throws ClassNotFoundException, SQLException {

		System.out.println("Return all hotels (id and name) whose currency is USD and HighRate greater than the ENTERED_VALUE\n\nPlease enter a HighRate");
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please enter HighRate:");
		int HighRate = scanner.nextInt();
		System.out.println("Entered HighRate="+HighRate);
		preparedStatement(HighRate);

		System.out.println("Please enter EANHotelID to find Hotel:");
		int EAN = scanner.nextInt();
		System.out.println("Entered EANHotelID="+EAN);
		preparedStatement_two(EAN);

		preparedStatement_three(EAN);
		}



	private static void preparedStatement(int HighRate)
 throws ClassNotFoundException, SQLException {
	
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
		con = getConnection();	//ej helt hundra hur inputen ska wrappas av tecken
		pstmt = con.prepareStatement(
		"SELECT EANHotelID, Name, PropertyCurrency, HighRate " +
		"FROM Property " +
		"WHERE HighRate > ? " +
		"ORDER BY HighRate;"
		);
		pstmt.setInt(1, HighRate);
		rs = pstmt.executeQuery();

		while(rs.next()){

			System.out.println("EANHotelID="+rs.getInt("EANHotelID")+",HotelName="+rs.getString("Name")+",PropertyCurrency="+rs.getString("PropertyCurrency")+",HighRate="+rs.getInt("HighRate"));
		}
		}finally{
			rs.close();
			pstmt.close();
			con.close();
		}

	}





	private static void preparedStatement_two(int EAN)
 throws ClassNotFoundException, SQLException {
int plus_hundra = 0;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
/*		try{
		con = getConnection(); //ej helt hundra hur inputen ska wrappas av tecken
		pstmt = con.prepareStatement(	
		"SELECT EANHotelID, Name, PropertyCurrency, HighRate " +
		"FROM Property " +
		"WHERE EANHotelID = ? ;"
		);

		pstmt.setInt(1, EAN);
		rs = pstmt.executeQuery();
		while(rs.next()){
			System.out.println("EANHotelID="+rs.getInt("EANHotelID")+",HotelName="+rs.getString("Name")+",PropertyCurrency="+rs.getString("PropertyCurrency")+",HighRate="+rs.getInt(4));
	
			System.out.println("100 will be added to the HighRate of the entered EANHotelID value");
			plus_hundra = rs.getInt("HighRate") + 100;
		}
		}finally{
			rs.close();
			pstmt.close();
			con.close();
		}
*/

		try{
		con = getConnection();
		pstmt = con.prepareStatement(
		"UPDATE Property " +
                "SET HighRate = HighRate+100 " +
		"WHERE EANHotelID = ? ;"
		);
		//pstmt.setInt(1, plus_hundra);
		pstmt.setInt(1, EAN);

		pstmt.executeUpdate();

//		System.out.println("EANHotelID="+rs.getInt("EANHotelID")+",HotelName="+rs.getString("Name")+",PropertyCurrency="+rs.getString("PropertyCurrency")+",HighRate="+rs.getInt("HighRate"));
		}finally{
			if(rs != null) rs.close();
			pstmt.close();
			con.close();
		}

	}

	private static void preparedStatement_three(int EAN) throws
ClassNotFoundException, SQLException {

		PreparedStatement pstmt = null;
		Connection con = null;
		ResultSet rs = null;

		try{
			con = getConnection();
			pstmt = con.prepareStatement(
				"SELECT EANHotelID, Name, HighRate, PropertyCurrency " +
				"FROM Property " +
				"WHERE EANHotelID = ? ;"
			);
			pstmt.setInt(1, EAN);

			rs=pstmt.executeQuery();

			while(rs.next()){
				System.out.println("EANHotelID="+rs.getInt("EANHotelID")+", Name="+rs.getString("Name")+" ,New HighRate="+rs.getInt("HighRate")+", Currency="+rs.getString("PropertyCurrency"));
			}
		}finally{
			if(rs != null) rs.close();
			pstmt.close();
			if(con != null) con.close();
		}
	}

        private static Connection getConnection() throws
ClassNotFoundException, SQLException {

                Connection con = null;

                // load the Driver Class
                Class.forName(DB_DRIVER_CLASS);

                // create the connection now
                con = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);

                System.out.println("DB Connection created successfully");
                return con;
        }

}
