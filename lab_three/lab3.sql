CREATE TABLE ht16_musicdb_it22.Composer (
id INT auto_increment,
first_name VARCHAR(100),
last_name VARCHAR(100),
born INT,
died INT,
primary key(id)
);

CREATE TABLE ht16_musicdb_it22.Instrument(
id INT auto_increment,
name VARCHAR(100),
PRIMARY KEY(id)
);

CREATE TABLE ht16_musicdb_it22.Location (
id INT auto_increment,
name VARCHAR(100),
PRIMARY KEY(id)
);

CREATE TABLE ht16_musicdb_it22.Publisher (
id INT auto_increment,
name VARCHAR(500),
PRIMARY KEY(id)
);

CREATE TABLE ht16_musicdb_it22.Title (
id INT auto_increment,
name VARCHAR(500),
comment VARCHAR(500),
ComposerId INT,
PRIMARY KEY(id),
FOREIGN KEY(ComposerId) REFERENCES ht16_musicdb_it22.Composer(id)
);

CREATE TABLE ht16_musicdb_it22.Title_Instrument (
TitleID INT,
InstrumentID INT,
NumberOfInstruments INT,
PRIMARY KEY(TitleID, InstrumentID),
FOREIGN KEY(TitleID) REFERENCES ht16_musicdb_it22.Title(id),
FOREIGN KEY(InstrumentID) REFERENCES ht16_musicdb_it22.Instrument(id)
);

CREATE TABLE ht16_musicdb_it22.Title_Location (
TitleID INT,
LocationID INT,
PRIMARY KEY(TitleID, LocationID),
FOREIGN KEY(TitleID) REFERENCES ht16_musicdb_it22.Title(id),
FOREIGN KEY(LocationID) REFERENCES ht16_musicdb_it22.Location(id)
);

CREATE TABLE ht16_musicdb_it22.Title_Publisher (
TitleID INT,
PublisherID INT,
PRIMARY KEY(TitleID, PublisherID),
FOREIGN KEY(TitleID) REFERENCES ht16_musicdb_it22.Title(id),
FOREIGN KEY(PublisherID) REFERENCES ht16_musicdb_it22.Publisher(id)
);

CREATE TABLE ht16_musicdb_it22.Buffer (
First_Name VARCHAR(100),
Last_Name VARCHAR(100),
Born INT,
Died INT,
Title VARCHAR(500),
Publisher VARCHAR(500),
Location VARCHAR(100),
Comment	VARCHAR(500),
Instrument	VARCHAR(100),
Num_Instrument INT
);

LOAD DATA LOCAL INFILE '~/Downloads/archive.csv'
INTO TABLE Buffer
COLUMNS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

INSERT INTO ht16_musicdb_it22.Composer (first_name, last_name, born, died)
SELECT DISTINCT First_Name, Last_Name, Born, Died
FROM ht16_musicdb_it22.Buffer
WHERE First_Name IS NOT NULL;

ALTER TABLE Buffer
ADD COLUMN comp_id INT;

UPDATE Buffer, Composer
SET Buffer.comp_id = Composer.id
WHERE (Buffer.First_Name = Composer.first_name);

INSERT INTO ht16_musicdb_it22.Title (name, comment, ComposerID)
SELECT DISTINCT Title, Comment, comp_id
FROM ht16_musicdb_it22.Buffer;

INSERT INTO ht16_musicdb_it22.Instrument (name)
SELECT DISTINCT Instrument
FROM ht16_musicdb_it22.Buffer
WHERE Buffer.Instrument IS NOT NULL;

INSERT INTO ht16_musicdb_it22.Location (name)
SELECT DISTINCT Location
FROM ht16_musicdb_it22.Buffer
WHERE Buffer.Location IS NOT NULL;

INSERT INTO ht16_musicdb_it22.Publisher (name)
SELECT DISTINCT Publisher
FROM ht16_musicdb_it22.Buffer
WHERE Buffer.Publisher IS NOT NULL;

ALTER TABLE Buffer
ADD COLUMN instr_id INT;

UPDATE Buffer, Instrument
SET Buffer.instr_id = Instrument.id
WHERE (Buffer.Instrument = Instrument.name);

ALTER TABLE Buffer
ADD COLUMN loc_id INT;

UPDATE Buffer, Location
SET Buffer.loc_id = Location.id
WHERE (Buffer.Location = Location.name);

ALTER TABLE Buffer
ADD COLUMN publ_id INT;

UPDATE Buffer, Publisher
SET Buffer.publ_id = Publisher.id
WHERE (Buffer.Publisher = Publisher.name);

ALTER TABLE Buffer
ADD COLUMN title_id INT;

UPDATE Buffer, Title
SET Buffer.title_id = Title.id
WHERE (Buffer.Title = Title.name);

INSERT INTO ht16_musicdb_it22.Title_Publisher (TitleID, PublisherID)
SELECT DISTINCT title_id, publ_id
FROM ht16_musicdb_it22.Buffer
WHERE (publ_id IS NOT NULL AND title_id IS NOT NULL);

INSERT INTO ht16_musicdb_it22.Title_Location (TitleID, LocationID)
SELECT DISTINCT title_id, loc_id
FROM ht16_musicdb_it22.Buffer
WHERE (loc_id IS NOT NULL AND title_id IS NOT NULL);

INSERT INTO ht16_musicdb_it22.Title_Instrument (TitleID, InstrumentID, NumberOfInstruments)
SELECT DISTINCT title_id, instr_id, Num_Instrument
FROM ht16_musicdb_it22.Buffer
WHERE (instr_id IS NOT NULL AND title_id IS NOT NULL);